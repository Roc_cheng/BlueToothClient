package bluetooth.com.bluetoothclient;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

public class Flash extends AppCompatActivity {

    //动画显示时间
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    private ImageView welcomeImg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);

        //将动画设置为全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //隐藏动画标题栏
        getSupportActionBar().hide();

        //逐渐显示动画效果
        welcomeImg = (ImageView) this.findViewById(R.id.splashscreen);
        AlphaAnimation anima = new AlphaAnimation(0.3f, 1.0f);
        anima.setDuration(SPLASH_DISPLAY_LENGTH);            // 设置动画显示时间
        welcomeImg.startAnimation(anima);

        //动画渲染完成所要跳转的界面
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(Flash.this, MainActivity.class);
                Flash.this.startActivity(mainIntent);
                Flash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
